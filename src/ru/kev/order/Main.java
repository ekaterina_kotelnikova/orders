package ru.kev.order;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Класс позволяет определить, сколько фирма в наилучшем случае сможет заработать,
 * принимая более ценные заказы.
 *
 * @author Kotelnikova E.V. group15it20
 */

public class Main {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int[] orders = inputPrice();
        int time = inputTime();

        Arrays.sort(orders);
        System.out.println(Arrays.toString(orders));

        int[] expensive = countOf(orders, time);

        System.out.println(Arrays.toString(expensive) + "\n" + "Cтоимость наиболее ценных заказов: " + sum(expensive));
    }

    /**
     * Метод для суммирования ценных заказов
     * @param expensive массив, содержащий ценные заказы
     * @return сумма ценных заказов
     */
    private static int sum(int[] expensive) {
        int sum = 0;
        for (int i = 0; i <= expensive.length - 1; i++) {
            sum += expensive[i];
        }
        return sum;
    }

    /**
     * Метод для ввода с клавиатуры количества времени
     * @return количество времени
     */
    public static int inputTime() {
        System.out.println("Введите количество времени: ");
        int time = scanner.nextInt();
        return time;
    }

    /***
     * Метод для ввода с клавиатуры стоимости заказов
     * @return массив со стоимостью заказов
     */
    public static int[] inputPrice() {
        System.out.println("Введите количество заказов: ");
        int countOfOrders = scanner.nextInt();
        System.out.println("Введите стоимость заказов: ");
        int[] orders = new int[countOfOrders];
        for (int i = 0; i <= orders.length - 1; i++) {
            orders[i] = scanner.nextInt();
        }
        return orders;
    }

    /**
     * Метод, который отбирает наиболее ценные заказы, основываясь на количество времени
     *
     * @param array массив, содержащий стоимость заказов
     * @param time  время, которое имеется в распоряжении фирмы
     * @return массив наиболее ценных заказов
     */

    public static int[] countOf(int[] array, int time) {
        int[] expensive = new int[time];
        if (time > array.length) {
            time = array.length;
        }

        for (int i = 0; i < time; i++) {
            expensive[i] = array[array.length - i - 1];
        }
        return expensive;
    }
}
